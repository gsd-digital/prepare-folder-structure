FasdUAS 1.101.10   ��   ��    k             i         I     �� 	��
�� .aevtodocnull  �    alis 	 o      ���� 0 
thefolders 
theFolders��    X      
��  
 n       I    �� ���� 0 preparefolder PrepareFolder   ��  c        o    ���� 0 	thefolder 	theFolder  m    ��
�� 
TEXT��  ��     f    �� 0 	thefolder 	theFolder  o    ���� 0 
thefolders 
theFolders      l     ��������  ��  ��        i        I     ������
�� .aevtoappnull  �   � ****��  ��    I    �� ��
�� .sysodlogaskr        TEXT  m        �   & D r o p   F o l d e r   o n   I c o n��        l     ��������  ��  ��        i         I      �� !���� 0 preparefolder PrepareFolder !  "�� " o      ���� 0 	thefolder 	theFolder��  ��     k    + # #  $ % $ l     ��������  ��  ��   %  &�� & O    + ' ( ' k   * ) )  * + * r     , - , n    
 . / . 1    
��
�� 
pnam / 4    �� 0
�� 
alis 0 o    ���� 0 	thefolder 	theFolder - o      ���� 0 thesendname theSendName +  1 2 1 Q    9 3 4 5 3 r    # 6 7 6 I   !���� 8
�� .corecrel****      � null��   8 �� 9 :
�� 
kocl 9 m    ��
�� 
cfol : �� ; <
�� 
insh ; 4    �� =
�� 
alis = o    ���� 0 	thefolder 	theFolder < �� >��
�� 
prdt > K     ? ? �� @��
�� 
pnam @ m     A A � B B  0 1 _ O r i g i n a l��  ��   7 o      ���� 0 	tmpfolder 	tmpFolder 4 R      ������
�� .ascrerr ****      � ****��  ��   5 k   + 9 C C  D E D r   + 2 F G F c   + 0 H I H l  + . J���� J b   + . K L K o   + ,���� 0 	thefolder 	theFolder L m   , - M M � N N  0 1 _ O r i g i n a l��  ��   I m   . /��
�� 
TEXT G o      ���� 0 	tmpfolder 	tmpFolder E  O�� O r   3 9 P Q P 4   3 7�� R
�� 
alis R o   5 6���� 0 	tmpfolder 	tmpFolder Q o      ���� 0 	tmpfolder 	tmpFolder��   2  S T S l  : :��������  ��  ��   T  U V U l  : :��������  ��  ��   V  W X W l   : :�� Y Z��   Y � �try			set sendFolder to make new folder at alias theFolder with properties {name:theSendName}		on error			set sendFolder to (theFolder & theSendName) as string			set sendFolder to alias sendFolder		end try    Z � [ [�  t r y  	 	 	 s e t   s e n d F o l d e r   t o   m a k e   n e w   f o l d e r   a t   a l i a s   t h e F o l d e r   w i t h   p r o p e r t i e s   { n a m e : t h e S e n d N a m e }  	 	 o n   e r r o r  	 	 	 s e t   s e n d F o l d e r   t o   ( t h e F o l d e r   &   t h e S e n d N a m e )   a s   s t r i n g  	 	 	 s e t   s e n d F o l d e r   t o   a l i a s   s e n d F o l d e r  	 	 e n d   t r y  X  \ ] \ l  : :��������  ��  ��   ]  ^ _ ^ l  : :��������  ��  ��   _  ` a ` l  : :��������  ��  ��   a  b c b Y   : � d�� e f g d k   K � h h  i j i r   K V k l k c   K T m n m n   K R o p o 4   O R�� q
�� 
cobj q o   P Q���� 0 x   p 4   K O�� r
�� 
alis r o   M N���� 0 	thefolder 	theFolder n m   R S��
�� 
TEXT l o      ���� 0 thefile theFile j  s�� s Z   W � t u���� t F   W � v w v F   W � x y x F   W r z { z >   W a | } | n   W ] ~  ~ 1   [ ]��
�� 
pnam  4   W [�� �
�� 
alis � o   Y Z���� 0 thefile theFile } m   ] ` � � � � �  0 1 _ O r i g i n a l { >   d n � � � n   d j � � � l 	 h j ����� � 1   h j��
�� 
pnam��  ��   � 4   d h�� �
�� 
alis � o   f g���� 0 thefile theFile � m   j m � � � � �  0 2 _ W o r k i n g y >   u  � � � n   u { � � � l 	 y { ����� � 1   y {��
�� 
pnam��  ��   � 4   u y�� �
�� 
alis � o   w x���� 0 thefile theFile � m   { ~ � � � � �  0 3 _ T o   S e n d w >   � � � � � n   � � � � � l 	 � � ����� � 1   � ���
�� 
pnam��  ��   � 4   � ��� �
�� 
alis � o   � ����� 0 thefile theFile � o   � ����� 0 thesendname theSendName u k   � � � �  � � � r   � � � � � c   � � � � � n   � � � � � 1   � ���
�� 
pnam � 4   � ��� �
�� 
alis � o   � ����� 0 thefile theFile � m   � ���
�� 
TEXT � o      ����  0 movefoldername moveFolderName �  � � � l  � ��� � ���   � 	 try    � � � �  t r y �  � � � l  � ��� � ���   � _ Y	set newMoveFolder to make new folder at sendFolder with properties {name:moveFolderName}    � � � � � 	 s e t   n e w M o v e F o l d e r   t o   m a k e   n e w   f o l d e r   a t   s e n d F o l d e r   w i t h   p r o p e r t i e s   { n a m e : m o v e F o l d e r N a m e } �  � � � l  � ��� � ���   �  end try    � � � �  e n d   t r y �  ��� � I  � ��� � �
�� .coremoveobj        obj  � o   � ����� 0 thefile theFile � �� ���
�� 
insh � o   � ����� 0 	tmpfolder 	tmpFolder��  ��  ��  ��  ��  �� 0 x   e l  = E ����� � I  = E�� ���
�� .corecnte****       **** � 4   = A�� �
�� 
alis � o   ? @���� 0 	thefolder 	theFolder��  ��  ��   f m   E F����  g m   F G������ c  � � � Q   � � � ��� � r   � � � � � I  � ����� �
�� .corecrel****      � null��   � � � �
� 
kocl � m   � ��~
�~ 
cfol � �} � �
�} 
insh � 4   � ��| �
�| 
alis � o   � ��{�{ 0 	thefolder 	theFolder � �z ��y
�z 
prdt � K   � � � � �x ��w
�x 
pnam � m   � � � � � � �  0 2 _ W o r k i n g�w  �y   � o      �v�v 0 	tmpfolder 	tmpFolder � R      �u�t�s
�u .ascrerr ****      � ****�t  �s  ��   �  � � � Q   � � ��r � k   � � �  � � � r   � � � � � I  � ��q�p �
�q .corecrel****      � null�p   � �o � �
�o 
kocl � m   � ��n
�n 
cfol � �m � �
�m 
insh � 4   � ��l �
�l 
alis � o   � ��k�k 0 	thefolder 	theFolder � �j ��i
�j 
prdt � K   � � � � �h ��g
�h 
pnam � m   � � � � � � �  0 3 _ T o   S e n d�g  �i   � o      �f�f 0 	tmpfolder 	tmpFolder �  � � � r   � � � � � I  � ��e�d �
�e .corecrel****      � null�d   � �c � �
�c 
kocl � m   � ��b
�b 
cfol � �a � �
�a 
insh � o   � ��`�` 0 	tmpfolder 	tmpFolder � �_ ��^
�_ 
prdt � K   � � � � �] ��\
�] 
pnam � m   � � � � � � � H   W a l b u s c h   -   O u t p u t _ P i c t u r e _ C o r r e c t e d�\  �^   � o      �[�[ 0 
tmpfolder2 
tmpFolder2 �  ��Z � r   � � � � I  ��Y�X �
�Y .corecrel****      � null�X   � �W � �
�W 
kocl � m  �V
�V 
cfol � �U � �
�U 
insh � o  �T�T 0 	tmpfolder 	tmpFolder � �S ��R
�S 
prdt � K   � � �Q ��P
�Q 
pnam � m  	 � � � � � 0   W a l b u s c h   -   O u t p u t _ E - C o m�P  �R   � o      �O�O 0 
tmpfolder3 
tmpFolder3�Z   � R      �N�M�L
�N .ascrerr ****      � ****�M  �L  �r   �  � � � l �K�J�I�K  �J  �I   �  � � � l �H�G�F�H  �G  �F   �  � � � Q  ( � �E � l �D�D   \ Vset tmpFolder to make new folder at alias theFolder with properties {name:"04_Sample"}    � � s e t   t m p F o l d e r   t o   m a k e   n e w   f o l d e r   a t   a l i a s   t h e F o l d e r   w i t h   p r o p e r t i e s   { n a m e : " 0 4 _ S a m p l e " }  R      �C�B�A
�C .ascrerr ****      � ****�B  �A  �E   � �@ l ))�?�>�=�?  �>  �=  �@   ( m     �                                                                                  MACS  alis    t  Macintosh HD               �Ŋ5H+   9
Finder.app                                                      A��v!d        ����  	                CoreServices    ��'�      �u��     9 f� f�  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  ��     l     �<�;�:�<  �;  �:   �9 l     �8�7�6�8  �7  �6  �9       �5	
�5  	 �4�3�2
�4 .aevtodocnull  �    alis
�3 .aevtoappnull  �   � ****�2 0 preparefolder PrepareFolder
 �1 �0�/�.
�1 .aevtodocnull  �    alis�0 0 
thefolders 
theFolders�/   �-�,�- 0 
thefolders 
theFolders�, 0 	thefolder 	theFolder �+�*�)�(�'
�+ 
kocl
�* 
cobj
�) .corecnte****       ****
�( 
TEXT�' 0 preparefolder PrepareFolder�.  �[��l kh )��&k+ [OY�� �& �%�$�#
�& .aevtoappnull  �   � ****�%  �$      �"
�" .sysodlogaskr        TEXT�# �j  �!  � ���! 0 preparefolder PrepareFolder�  ��   �� 0 	thefolder 	theFolder�   ��������� 0 	thefolder 	theFolder� 0 thesendname theSendName� 0 	tmpfolder 	tmpFolder� 0 x  � 0 thefile theFile�  0 movefoldername moveFolderName� 0 
tmpfolder2 
tmpFolder2� 0 
tmpfolder3 
tmpFolder3 ������ A����
 M�	�� � �� �� � � � �
� 
alis
� 
pnam
� 
kocl
� 
cfol
� 
insh
� 
prdt� 
� .corecrel****      � null�  �
  
�	 
TEXT
� .corecnte****       ****
� 
cobj
� 
bool
� .coremoveobj        obj �,�(*�/�,E�O *���*�/���l� 	E�W X 
 ��%�&E�O*�/E�O u*�/j kih *�/�/�&E�O*�/�,a 	 *�/�,a a &	 *�/�,a a &	 *�/�,�a & *�/�,�&E�O��l Y h[OY��O *���*�/��a l� 	E�W X 
 hO @*���*�/��a l� 	E�O*�����a l� 	E�O*�����a l� 	E�W X 
 hO hW X 
 hOPU ascr  ��ޭ